import React from 'react';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from './components/Common/Navbar';
import Home from './components/Common/Home';
import Footer from './components/Common/Footer';
import Clothes, {image,title,price} from './components/Pages/Clothes';
import Textiles from './components/Pages/Textiles';
import Cosmetics from './components/Pages/Cosmetics';
import Contact from './components/Common/Contact';
import Details from './components/Common/Details';
import Terms from './components/Common/Terms';
import Add from './components/Pages/Sell/Add';
import Profile from './components/Pages/Profile';
import {Login} from './components/Pages/Login';
import { SignUp } from './components/Pages/SignUp';
import EditProfile from './components/Pages/EditProfile';
import ForgotPassword from './components/Pages/ForgotPassword';
import OTPVerification from './components/Pages/OTPVerification';




const App = () => {
  return (
    <>
    <Routes>
      <Route exact index path="/" element={<Login/>}/>
      <Route exact path="SignUp" element={<SignUp/>}/>
      <Route exact path = "forgotPassword" element = {<ForgotPassword/>}/>
      <Route exact path = "otpverification" element = {<OTPVerification/>}/>
      <Route exact path="snapshop" element={<Navbar/>}>
        <Route path="/snapshop/home" element={<Home/>}/>
        <Route exact index element={<Home/>}/>
        <Route path="/snapshop/home/Clothes" element={<Clothes/>}/>
        <Route path="/snapshop/home/Terms" element={<Terms/>}/>
        <Route path="/snapshop/Clothes" element={<Clothes/>}/>
        <Route exact path="Cosmetics" element={<Cosmetics/>}/>
        <Route exact path="Textiles" element={<Textiles/>}/>
        <Route exact path="Contact" element={<Contact/>}/>
        <Route exact path="Terms" element={<Terms/>}/>
        <Route exact path="Footer" element={<Footer/>}/>
        <Route exact path="Profile" element={<Profile/>}/>
        <Route exact path="EditProfile" element={<EditProfile/>}/>
        <Route path="/snapshop/add" element={<Add />} />
        <Route exact path="Clothes/:id" element={<Details/>}/>
        <Route exact path="Cosmetics/:id" element={<Cosmetics/>}/>
        <Route exact path="Textiles/:id" element={<Textiles/>}/>
      </Route>
    </Routes>
    </>
  )
} 

export default App;

