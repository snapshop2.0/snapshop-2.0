import React from "react";
import { useState } from "react";

const Editprofile = () => {
    const [isEditing, setIsEditing] = useState(false);
    const [userName, setUserName] = useState("");
    const [userPhoneNumber, setUserPhoneNumber] = useState("");
    const [userEmail, setUserEmail] = useState("");
    const [userGender, setUserGender] = useState("Male");
    const [userImage, setUserImage] = useState(null);

    const handleSave = (e) => {
        e.preventDefault();
        setIsEditing(false);
    };

    const handleEdit = () => {
        setIsEditing(true);
    };

    const handleImageChange = (e) => {
        const file = e.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onloadend = () => {
                setUserImage(reader.result);
            };
        }
    };
    const imageStyle = {
        width: "150px",
        height: "150px",
        objectFit: "cover",
        borderRadius: "50%",
        marginBottom: "20px"
    };

    const inputStyle = {
        display: "block",
        width: "100%",
        padding: "10px",
        marginTop: "5px",
        border: "none",
        backgroundColor: "#E5E5E5",
        color: "#000",
        borderRadius: "4px",
        boxSizing: "border-box"
    };

    const editButtonStyle = {
        backgroundColor: "#e3bf8d",
        color: "#FFF",
        padding: "10px 15px",
        border: "none",
        borderRadius: "4px",
        marginTop: "20px",
        cursor: "pointer"
    };
    const detailsStyle = {
        backgroundColor: "#FFF",
        padding: "20px",
        boxShadow: "0 0 5px rgba(0,0,0,0.3)",
        borderRadius: "10px",
        textAlign: "center",
        width: "25%",
        margin: "0 auto"
    };
    return (
        <>
            <div style={{ display: "flex", alignItems: "center", justifyContent: "center", height: "calc(100vh - 100px)" }}>
                {isEditing ? (
                    // Edit mode
                    <form style={detailsStyle}>
                        <label htmlFor="user-image" style={{ display: "block" }}>Profile Image</label>
                        <input
                            type="file"
                            id="user-image"
                            onChange={handleImageChange}
                            style={{ marginBottom: "20px" }}
                        />
                        <br />
                        <img
                            src={userImage || "https://via.placeholder.com/150"}
                            alt="User"
                            style={{ ...imageStyle, borderRadius: "4px" }}
                        />
                        <br />
                        <label htmlFor="user-name" style={{ marginTop: "20px" }}>Name</label>
                        <input

                            type="text"
                            id="user-name"
                            value={userName}
                            onChange={(e) => setUserName(e.target.value)}
                            placeholder="Enter name"
                            style={inputStyle}
                        />

                        <label htmlFor="user-phone-number">Phone Number</label>
                        <input
                            type="tel"
                            id="user-phone-number"
                            value={userPhoneNumber}
                            onChange={(e) => setUserPhoneNumber(e.target.value)}
                            placeholder="Enter phone number"
                            style={inputStyle}
                        />

                        <label htmlFor="user-email">Email</label>
                        <input
                            type="email"
                            id="user-email"
                            value={userEmail}
                            onChange={(e) => setUserEmail(e.target.value)}
                            placeholder="Enter email"
                            style={inputStyle}
                        />

                        <label htmlFor="user-gender">Gender</label>
                        <select
                            id="user-gender"
                            value={userGender}
                            onChange={(e) => setUserGender(e.target.value)}
                            style={inputStyle}
                        >
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                            <option value="Other">Other</option>
                        </select>

                        <button onClick={handleSave} style={editButtonStyle}>Save</button>
                    </form>
                ) : (
                    // View mode

                    <div style={detailsStyle}>
                        <img
                            src={userImage || "https://via.placeholder.com/150"}
                            alt="User"
                            style={{ ...imageStyle, borderRadius: "4px" }}
                        />

                        <p style={{ marginBottom: "5px" }}>Name</p>
                        <p style={inputStyle}>{userName || "Your name"}</p>

                        <p style={{ marginBottom: "5px" }}>Phone Number:</p>
                        <p style={inputStyle}>{userPhoneNumber || "Enter phone number"}</p>

                        <p style={{ marginBottom: "5px" }}>Email:</p>
                        <p style={inputStyle}>{userEmail || "Enter email"}</p>

                        <p style={{ marginBottom: "5px" }}>Gender:</p>
                        <p style={inputStyle}>{userGender}</p>

                        <button onClick={handleEdit} style={editButtonStyle}>Edit</button>
                    </div>
                )}
            </div>
        </>
    )
}

export default Editprofile;