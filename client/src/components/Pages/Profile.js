import React, { useEffect, useState } from "react";
import { useNavigate,useParams } from "react-router-dom";
import axios from "axios";

const API_BASE_URL = 'http://localhost:5000';


const Profile = () => {
  const navigate = useNavigate();
  const uid = parseInt(localStorage.getItem("uid"));

  const [showModal, setShowModal] = useState(false);
  const [productIdToDelete, setProductIdToDelete] = useState(null);
  const blockStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#f0dcce",
    padding: "30px",
    width: "100%",
    margin: "0",
    borderBottom: "1px solid #ccc"
  };

  const handleAddItemClick = () => {
    navigate("/snapshop/add");
  };
  const handleChangePasswordClick = () => {
    navigate("/snapshop/editprofile");
  };


  const [datas,setDatas]=useState('');
  const getYourProduct=async()=>{
    try{
      await axios.get(`${API_BASE_URL}/getYourProduct/${uid}`)
      .then(res=>{
        setDatas(res.data)
      })
    }catch(error){
      console.log(error);
    }
  }

  const handleDelete = async (pid) => {
    setProductIdToDelete(pid);
    setShowModal(true);
  };

  const confirmDelete = async () => {
    try {
      await axios.post(`${API_BASE_URL}/deleteProduct/${productIdToDelete}`, { uid });
      // Perform any additional actions after successful deletion
      // For example, you can refresh the product list by calling getYourProduct()
      getYourProduct();
    } catch (error) {
      console.log(error);
    }
    setShowModal(false);
  };

  useEffect(()=>{
    getYourProduct();
  },[datas])
  return (
    <div>
      <div style={blockStyle}>
        <div>
          <h1 style={{ color: "#000", fontSize: "28px" }}>Profile</h1>
        </div>
        <div>
          <button
            className="fa-light fa-circle-plus p-3"
            style={{ color: "#fd8231", fontSize:15}}
            onClick={handleAddItemClick}
          >
            Add items
          </button>
         
        </div>
        <div>
          <button
            className="fa-light fa-circle-plus p-3"
            style={{ color: "#fd8231", fontSize:15 }}
            onClick={handleChangePasswordClick}
          >
            Change Password
          </button>
         
        </div>

        {showModal && (
        <div className="modal" style={{ display: "block", backgroundColor: "rgba(0, 0, 0, 0.5)", position: "fixed", top: 0, left: 0, right: 0, bottom: 0 }}>
          <div className="modal-dialog" style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)" }}>
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Confirmation</h5>
                <button type="button" className="close" onClick={() => setShowModal(false)}>
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <p>Are you sure you want to delete this product?</p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" onClick={() => setShowModal(false)}>Cancel</button>
                <button type="button" className="btn btn-danger" onClick={confirmDelete}>Delete</button>
              </div>
            </div>
          </div>
        </div>
      )}
      </div>
      <div className="product-list" style={{display:'flex',flexDirection:'row',flexWrap:'wrap', justifyContent:'center'}}>
      {datas && datas.length>0 ? datas.map((data) => (
        <div key={data.id} className="product">
          <img src={`${API_BASE_URL}/uploads/${data.image}`} alt={data.name} style={{width:200,height:'auto'}} className="product-image" />
          <h2 className="product-name">{data.name}</h2>
          <p className="product-price">Price: {data.price}</p>
          <p className="product-quantity">Quantity: {data.quantity}</p>
          <p className="product-location">Location: {data.location}</p>
          <p className="product-description">Description: {data.description}</p>
          <p className="product-number">Number: {data.number}</p>
          <button className="btn btn-danger btn-sm" onClick={()=>handleDelete(data.pid)}>Delete</button>
        </div>
      )):'No Product Items!'}
    </div>
    
    </div>
  );
};

export default Profile;
