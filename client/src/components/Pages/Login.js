import React, { useState } from "react";
import { Container, Form, Button } from 'react-bootstrap';
import { Link, useNavigate } from "react-router-dom";
import snapshop from "../../assests/SnapShop.png"
import './Login.css';

export const Login = (props) => {
  const navigate = useNavigate();
  const [email, setEmail] = useState('');
  const [password, setPass] = useState('');
  const [errors, setErrors] = useState('');

  async function handleSubmit(e) {
    e.preventDefault();
    const data = { email, password };

    const response = await fetch(`http://localhost:5000/auth/login`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });

    const parseRes = await response.json();

    if (response.ok && parseRes.token) {
      localStorage.setItem("token", parseRes.token);
      localStorage.setItem("uid", parseRes.uid);
      navigate('/snapshop/Home');
    }

    if (!response.ok) {
      setErrors(parseRes);
    }
  }

  return (
    <Container fluid className="login-container">
      <div className="form-container">
        <Form className="login-form" onSubmit={handleSubmit}>
          <h2 className="form-title">Log In</h2>
          <Form.Group controlId="formBasicEmail">
            <Form.Label className="form-label">Email</Form.Label>
            <Form.Control
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              type="email"
              placeholder="Your Email"
              id="email"
              name="email"
            />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label className="form-label">Password</Form.Label>
            <Form.Control
              value={password}
              onChange={(e) => setPass(e.target.value)}
              type="password"
              placeholder="******"
              id="password"
              name="password"
            />
          </Form.Group>

          {errors && <div className="error">{errors}</div>}
          <Button type="submit" className="login-button">Log In</Button>
          <div className="register-link">
            <Link to={'ForgotPassword'}>Forgot Password?</Link>
          </div>
        </Form>

      </div>
      <div className="image-container">
        <Form className="image">
          <h2 className="image-title">New Here?</h2>
          <p className="image-des">Sign up and discover a greate <br />
            amount of new opportunities!</p>
          <div className="register-link">
            <Link to={'SignUp'}>Sign up here</Link>
          </div>
          <div className="image-logo">
            <img
              src={snapshop}

            />
          </div>
        </Form>
      </div>
    </Container>
  );
};
