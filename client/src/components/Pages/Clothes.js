import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Modal } from 'react-bootstrap';
import Heart from 'react-animated-heart';
import Footer from '../Common/Footer';

const API_BASE_URL = 'http://localhost:5000';

const Clothes = () => {
  const [clotheItem, setClotheItem] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [isClick, setClick] = useState(false);
  useEffect(() => {
    getClothes();
  }, []);

  const getClothes = async () => {
    try {
      const res = await axios.get(`${API_BASE_URL}/getProducts/clothes`);
      setClotheItem(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleProductClick = (product) => {
    setSelectedProduct(product);
  };
  
  const handleCloseModal = () => {
    setSelectedProduct(null);
  };

  <img src=""/>
  const bgimage = require('../../assests/cosemetic.jpg');

  return (
    <>
      <section
        className="hero-section border border-dark"
        style={{ backgroundImage: `url(${bgimage})` }}
      >
        <div className="container">
          <div className="row d-flex align-items-center justify-content-end">
            <div className="hero-text col-12 col-lg-6">
              <h1>Clothes</h1>
            </div>
            <div className="col-lg-3"></div>
            <div className="col-lg-3">
              <h3>IF YOU CAN'T STOP THINKING ABOUT IT, BUY IT</h3>
              <p className="main-hero-para">Today's Recommended Products</p>
            </div>
          </div>
        </div>
      </section>

      <div className="container">
        <div className="row items-margin-left">
          {clotheItem.map((item) => {
            const { pid, image, name, price, description } = item;
            return (
              <div className="item-box category col-12 col-lg-3" key={pid}>
                <Link
                  className="product-render"
                  onClick={() => handleProductClick(item)}
                >
                  <img
                    src={`${API_BASE_URL}/uploads/${image}`}
                    alt="clothe"
                  />
                  <h2 className="item-name">{name}</h2>
                  <h3 className="price">{price}</h3>
                </Link>
              </div>
            );
          })}
        </div>
      </div>

      <Modal show={selectedProduct !== null} onHide={handleCloseModal} size="lg">
        {selectedProduct && (
          <>
            <Modal.Header closeButton>
              <Modal.Title>{selectedProduct.name}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="row justify-content-center">
                <div className="col-8">
                  <img
                    src={`${API_BASE_URL}/uploads/${selectedProduct.image}`}
                    alt="clothe"
                    style={{maxWidth:'400px'}}
                  />
                </div>
                <div className="col-4">
                  <h3>{selectedProduct.price}</h3>
                  <p>{selectedProduct.description}</p>
                  <Heart isClick={isClick} onClick={() => setClick(!isClick)} />
                </div>
                
              </div>
            </Modal.Body>
          </>
        )}
      </Modal>
      <Footer/>
    </>
  );
};

export default Clothes;
