import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

export default function ForgotPassword() {
  const navigate = useNavigate();
  const [email, setEmail] = useState('');

  function validEmail(userEmail) {
    return /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(userEmail);
  }

  const sendMail = async (event) => {
    event.preventDefault();

    if (!email) {
      console.log('No email');
      return;
    } else if (!validEmail(email)) {
      console.log('Not a valid email');
      return;
    }

    const data = { email };
    const sendingMail = await fetch('http://localhost:5000/sendPassword', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data),
    });

    const parseRes = await sendingMail.json();

    if (parseRes === 'false') {
      // Handle error
    } else {
      setTimeout(() => {
        navigate('/');
      }, 2000);
    }

    console.log(typeof parseRes);
  };

  return (
    <div
      style={{
        minHeight: '50vh',
        Width: '50%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        border: '1px solid #FE4C00', // Apply border
        borderRadius: '10px', // Apply border radius
        padding: '20px' // Add padding
      }}
    >
      <div className="text-center">
        <h2 style={{ color: '#FE4C00' }}>Forgot Your Password?</h2>
        <h6 className="mb-5" style={{ color: '#FE4C00' }}>
          Please enter your recovery email address below to reset your password.
        </h6>

        <form className="mt-3" onSubmit={sendMail}>
          <input
            className="form-control"
            placeholder="12200005.gcit@rub.edu.bt"
            type="email"
            value={email}
            onChange={(event) => setEmail(event.target.value)}
            style={{ border: '2px solid #FE4C00', borderRadius: '5px', padding: '10px' }}
          />
          <button className="btn text-white px-4 mt-3" style={{ backgroundColor: '#FE4C00' }}>
            Get Password
          </button>
        </form>
      </div>
    </div>
  );
}
