import React, { useState} from "react";
import { Link, useNavigate } from "react-router-dom";

export const SignUp = (props) => {

    const navigate = useNavigate();
    
    const [email, setEmail] = useState('');
    const [password, setPass] = useState('');
    const [confirm_password,setCpass]=useState('');
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [gender, setGender] = useState('');

    const [errors, setErrors] = useState('');
    const [userExist,setUserExist] = useState('');

    const gettingOTP = async (e) => {
      e.preventDefault();
      try {
          const body = {email:email,password,name:name,confirm:confirm_password,phone,gender};
          const response = await fetch("http://localhost:5000/sendOTP",{
              method:'POST',
              headers: {"Content-Type":"application/json"},
              body:JSON.stringify(body)
          })

          const parseRes = await response.json();
          console.log("otp received ",parseRes);
          if (parseRes.success === true) {
              navigate('/otpverification', {state:{otp:parseRes.otp,email:email,password,name:name,confirm:confirm_password,gender,phone}})
          }
      } catch (error) {
          console.log(error.message);
      }
  }


    const handleSubmit = async (e) => {
        e.preventDefault()
       
        const data = {email,name,phone,gender,password,confirm_password};

        const response = await fetch(`http://localhost:5000/auth/register`,{
          method:'POST',
          headers:{"Content-Type":"application/json"},
          body:JSON.stringify(data)
        })

        const parseRes = await response.json();

        if (!response.ok) {
          setErrors(parseRes);
        }

        if (parseRes.userExist) {
          setUserExist(parseRes.userExist)
        }

        if (response.ok && parseRes.token) {
          console.log("token",parseRes.token)
          setTimeout(() => {
            navigate('/')
          },2000)
        }
        
    }

    const handleGenderChange = (event) => {
        setGender(event.target.value);
      };

      console.log(name)
      console.log(email)

    return (
        <div className="auth-form-container " >
            <h2>Sign up</h2>
            <form className="signup-form" onSubmit={gettingOTP}>
                <label htmlFor="name">Full Name</label>
                <input  value={name} onChange={(e) => setName(e.target.value)} name="name" id="name" placeholder="Full Name"/>
                
                <label htmlFor="email"> Email </label>
                <input value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="your email" id="email" name="email"/>
                {(errors==="Email should not be empty" || errors==="Invalid email format") && <p style={{color: "red"}}>{errors}</p>}
                
                <label htmlFor="phone"> Phone Number </label>
                <input value={phone} onChange={(e) => setPhone(e.target.value)} type="tele" placeholder="your phone number" id="phone" name="phone"/>
                
                
                <label htmlFor="gender">Gender:</label>
                    <select id="gender" value={gender} onChange={handleGenderChange}>
                        <option value="select">Select</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                    </select>
        
                
                <label htmlFor="password"> Password </label>
                <input value={password} onChange={(e) => setPass(e.target.value)} type="password" placeholder="*****" id="password" name="password"/>
                {(errors==="Password should not be empty" || errors==="Password must be 8 characters") && <p style={{color: "red"}}>{errors}</p>}

                <label htmlFor="password"> Confrim Password </label>
                <input value={confirm_password} onChange={(e) => setCpass(e.target.value)} type="password" placeholder="*****" id="confrim_password" name="confrim_password"/>
                {errors==="Passwords do not match" && <p style={{color: "red"}}>{errors}</p>}

                {userExist &&
                  <div className="p-3">
                    {userExist}
                  </div>
                }
                <button className="btn">Sign up </button>
            </form>
           
            <Link to={'login'}> <button className="link-btn" >Already have an account? Login here</button></Link>
        </div>
    )
}