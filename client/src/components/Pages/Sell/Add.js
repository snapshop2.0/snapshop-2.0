import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import axios from 'axios';

const API_BASE_URL = 'http://localhost:5000'
const Add = () => {

  const uid = parseInt(localStorage.getItem("uid"));

  const [images, setImages] = useState('');
  const [title, setTitle] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState("");
  const [quantity, setQuantity] = useState("");
  const [location, setLocation] = useState("");
  const [description, setDescription] = useState("");
  const [number, setNumber] = useState("");
  // const [isDraft, setIsDraft] = useState(false);

  const [added,setAdded] = useState('');

  // const handleSaveDraft = () => {
  //   setIsDraft(true);
  //   // Save the product as a draft
  // };

  // const handlePublish = () => {
  //   // Publish the product
  // };

  // const handleCancel = () => {
  //   // Cancel adding the product
  // };
  console.log(uid);
  console.log(number);

  const handleSubmit = async () => {
    const formData = new FormData();
    formData.append('image', images);
    formData.append('title', title);
    formData.append('category', category);
    formData.append('quantity', quantity);
    formData.append('location', location);
    formData.append('description', description);
    formData.append('price', price);
    formData.append('owner', uid);
    formData.append('number', number);
    try {
      const res = await axios.post(`${API_BASE_URL}/uploadProducts`, formData);
      const { message } = res.data;
      setAdded(message)
      console.log(message);
    } catch (error) {
      console.log(error);
    }

  }

  const blockStyle = {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "#f0dcce",
    padding: "30px",
    width: "100%",
    margin: "0",
    borderBottom: "1px solid #ccc"
  };

  const containerStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "130vh",
    background: "#f9f9f9"
  };

  const formContainerStyle = {
    width: "500px",
    border: "1px solid #ccc",
    padding: "20px",
    background: "#fff",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)"
  };

  const buttonGroupStyle = {
    display: "flex",
    justifyContent: "space-between",
    marginTop: "15px"
  };

  const labelStyle = {
    fontSize: "14px",
    marginBottom: "5px",
    color: "black"
  };

  const inputStyle = {
    fontSize: "13px"
  };

  const buttonStyle = {
    fontSize: "13px",
    padding: "5px 10px"
  };

  return (
    <div>
      <div style={blockStyle}>
        <div>
          <h1 style={{ color: "#000", fontSize: "28px" }}>Add item</h1>
        </div>
      </div>
      <div style={containerStyle}>
        <div style={formContainerStyle}>
          <Form>
            <Form.Group controlId="formProductImages">
              <Form.Label style={labelStyle}>Product Images</Form.Label>
              <Form.Control
                type="file"
                // multiple
                onChange={(e) => setImages(e.target.files[0])}
                style={{ height: "100px" }}
              />
            </Form.Group>

            <Form.Group controlId="formProductTitle">
              <Form.Label style={labelStyle}>Title</Form.Label>
              <Form.Control
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                style={inputStyle}
              />
            </Form.Group>

            <Form.Group controlId="formProductCategory">
              <Form.Label style={labelStyle}>Category</Form.Label>
              <Form.Control
                as="select"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
                style={inputStyle}
              >
                <option value="">Select a category</option>
                <option value="Clothes">Clothes</option>
                <option value="Cosmetics">Cosmetics</option>
                <option value="Textile">Textile</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="formProductPrice">
              <Form.Label style={labelStyle}>Price</Form.Label>
              <Form.Control
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                style={inputStyle}
              />
            </Form.Group>

            <Form.Group controlId="formProductQuantity">
              <Form.Label style={labelStyle}>Quantity</Form.Label>
              <Form.Control
                type="number"
                value={quantity}
                onChange={(e) => setQuantity(e.target.value)}
                style={inputStyle}
              />
            </Form.Group>

            

            <Form.Group controlId="formProductDescription">
              <Form.Label style={labelStyle}>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                style={inputStyle}
              />
            </Form.Group>

            <Form.Group controlId="formProductLocation">
              <Form.Label style={labelStyle}>Location</Form.Label>
              <Form.Control
                as="select"
                value={location}
                onChange={(e) => setLocation(e.target.value)}
                style={inputStyle}
              >
              <option value="">Select Location</option>
              <option value="Bumthang">Bumthang</option>
              <option value="Chhuka">Chhuka</option>
              <option value="Dagana">Dagana</option>
              <option value="Gasa">Gasa</option>
              <option value="Haa">Haa</option>
              <option value="Lhuntse">Lhuntse</option>
              <option value="Mongar">Mongar</option>
              <option value="Paro">Paro</option>
              <option value="Pemagatshel">Pemagatshel</option>
              <option value="Punakha">Punakha</option>
              <option value="Samdrup Jongkhar">Samdrup Jongkhar</option>
              <option value="Samtse">Samtse</option>
              <option value="Sarpang">Sarpang</option>
              <option value="Thimphu">Thimphu</option>
              <option value="Tashigang">Tashigang</option>
              <option value="Tashiyangtse">Tashiyangtse</option>
              <option value="Trongsa">Trongsa</option>
              <option value="Tsirang">Tsirang</option>
              <option value="Wangdue Phodrang">Wangdue Phodrang</option>
              <option value="Zhemgang">Zhemgang</option>
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="formProductNumber">
              <Form.Label style={labelStyle}>Number</Form.Label>
              <Form.Control
                as="textarea"
                value={number}
                onChange={(e) => setNumber(e.target.value)}
                style={inputStyle}
              />
            </Form.Group>

            <div style={buttonGroupStyle}>
              {added && 
                <div style={{color:'green'}}>
                  {added}
                </div>
              }
              <Button variant="primary" onClick={handleSubmit} style={buttonStyle}>
                Publish
              </Button>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Add;
