import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { useNavigate } from 'react-router-dom';

export default function OTPVerification() {
    const navigate = useNavigate();
    const location = useLocation();

    const [OTP,setOTP] = useState("");
    const [enteredOTP,setEnteredOTP] = useState("");
    const [resendingOTP,setResendingOTP] = useState(false);
    const [registering,setRegistering] = useState(false);
    const [error,setError] = useState('');

    const {otp,email, name, password,confirm,phone,gender} = location.state;

    useEffect(() => {
        setOTP(otp);
    },[])

    const gettingOTP = async () => {
        setResendingOTP(true);
        try {
            const body = {email:email,password,name:name,confirm:confirm};
            const response = await fetch("http://localhost:5000/sendOTP",{
                method:'POST',
                headers: {"Content-Type":"application/json"},
                body:JSON.stringify(body)
            })

            const parseRes = await response.json();
            console.log(parseRes);
            if (parseRes.success === true) {
                setOTP(parseRes.otp);
                setResendingOTP(false)
            } else {
                setResendingOTP(false)
            }
        } catch (error) {
            console.log(error.message);
            setResendingOTP(false)
        }
    }

    const handleSubmit = async () => {
        const data = {email,name,phone,gender,password,confirm_password:confirm};

        const response = await fetch(`http://localhost:5000/auth/register`,{
          method:'POST',
          headers:{"Content-Type":"application/json"},
          body:JSON.stringify(data)
        })

        const parseRes = await response.json();

        if (!response.ok) {
            console.log(parseRes);
          setError(parseRes);
        }

        if (parseRes.userExist) {
            console.log(parseRes);
          setError(parseRes.userExist)
        }

        if (response.ok && parseRes.token) {
          console.log("token",parseRes.token)
          setTimeout(() => {
            navigate('/')
          },2000)
        }
        
    }

    const registerNow = async () => {
        setRegistering(true)
        if (enteredOTP === OTP) {
            handleSubmit();
        } else {
            setRegistering(false);
        }
    }

    console.log(OTP);
    console.log(enteredOTP)
  return (
    <div className='text-center d-flex justify-content-center align-items-center'>

        <div>
            <h5 className='text-white'>Please Check Your Mail.</h5>

            <div>
                <label className='text-white'>OTP</label>
                <input type='text' className='form-control'
                    value={enteredOTP}
                    onChange={(event) => setEnteredOTP(event.target.value)}
                    maxLength={4}
                />
            </div>
            <div className='mt-4 d-flex justify-content-between'>
                <button className="btn" style={{backgroundColor:'#FE4C00', color:'white'}} onClick={gettingOTP}>
                    {resendingOTP ? (
                        <div class="spinner-border text-success" role="status" style={{width:20,height:20}}>
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    ):"Resend"

                    }
                </button>
                <button className="btn" style={{backgroundColor:'#FE4C00', color:'white'}} onClick={registerNow}>Continue</button>
            </div>
        </div>
    </div>
  )
}
