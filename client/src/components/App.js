import React from 'react';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import Navbar from './Navbar';
import Home from './Home';
import Footer from './Footer';
import Clothes, {image,title,price} from './Clothes';
import Textiles from './Textiles';
import Cosmetics from './Cosmetics';
import Contact from './Contact';
import Details from './Details';
import Terms from './Terms';
import UserProfile from './UserProfile';
import {Login} from './Login';
import { SignUp } from './SignUp';
import ForgotPassword from './ForgotPassword';
import OTPVerification from './OTPVerification';



const App = () => {
  return (
    <>
    {/* <Router>
      <Route exact path="/Login"><Login/></Route>
      <Navbar />
        <Switch>
          <Route exact path="/"><Home /></Route>
          <Route exact path="/Clothes"><Clothes /></Route>
          <Route exact path="/Cosmetics"><Cosmetics /></Route>
          <Route exact path="/Textiles"><Textiles /></Route>
          <Route exact path="/Contact"><Contact /></Route>
          <Route exact path="/Terms"><Terms /></Route>
          <Route exact path="/UserProfile"><UserProfile /></Route>
          <Route exact path="/Clothes/:id"><Details /></Route>
          <Route exact path="/Cosmetics/:id"><Details /></Route>
          <Route exact path="/Textiles/:id"><Details /></Route>
        </Switch>
      <Footer />
    </Router> */}
    <Routes>
      <Route exact index path="/" element={<Login/>}/>
      <Route exact path="SignUp" element={<SignUp/>}/>
      <Route exact path = "forgotPassword" element = {<ForgotPassword/>}/>
      <Route exact path = "otpverification" element = {<OTPVerification/>}/>
      <Route exact path="snapshop" element={<Navbar/>}>
        <Route exact index element={<Home/>}/>
        <Route exact path="Clothes" element={<Clothes/>}/>
        <Route exact path="Cosmetics" element={<Cosmetics/>}/>
        <Route exact path="Textiles" element={<Textiles/>}/>
        <Route exact path="Contact" element={<Contact/>}/>
        <Route exact path="Terms" element={<Terms/>}/>
        <Route exact path="UserProfile" element={<UserProfile/>}/>
        <Route exact path="Clothes/:id" element={<Clothes/>}/>
        <Route exact path="Cosmetics/:id" element={<Cosmetics/>}/>
        <Route exact path="Textiles/:id" element={<Textiles/>}/>
      </Route>
    </Routes>
    </>
  )
} 

export default App;

