import React, { useState} from "react"
import { Link, useNavigate } from "react-router-dom";

export const Login = (props) => {

  const navigate = useNavigate();

    const [email, setEmail] = useState('');
    const [password, setPass] = useState('');

    const [errors, setErrors] = useState('');

   
     

    // function handleChange(e) {
    //     setValues({...values, [e.target.email]: e.target.value})
    // }

    async function handleSubmit(e) {
        e.preventDefault();
        
        const data = {email,password}
        const response = await fetch(`http://localhost:5000/auth/login`,{
          method:'POST',
          headers:{"Content-Type":"application/json"},
          body:JSON.stringify(data)
        })
        
        const parseRes = await response.json();

        if (response.ok && parseRes.token) {
          console.log(parseRes.token);
          localStorage.setItem("token",parseRes.token);
          navigate('snapshop')
        }

        if (!response.ok) {
          setErrors(parseRes);
        }
    }



    return (
        <div className="auth-form-container">
            <h2>Log in</h2>
            <form className="login-form" onSubmit={handleSubmit}>
                <label htmlfor="email"> Email </label>
                <input value={email} onChange={(e)=>setEmail(e.target.value)} type="email" placeholder="your email" id="email" name="email"/>
                <label htmlfor="password"> Password </label>
                <input value={password} onChange={(e)=>setPass(e.target.value)} type="password" placeholder="*****" id="password" name="password"/>

                <Link to="forgotPassword">forgot your password?</Link>

                {errors && 
                  <div>
                    {errors}
                  </div>
                }

                <button type="submit" class="btn">Log In </button>
            </form>
            <Link to={'SignUp'}> <button className="link-btn" >Don't have an account? Sign-up here</button></Link>
            
       </div>
    )
    
}