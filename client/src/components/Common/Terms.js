import React from 'react';

function Terms() {
    return (
        <>
            <meta charSet="UTF-8" />
            <title>Terms and Conditions</title>
            <link
                href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                rel="stylesheet"
                integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                crossOrigin="anonymous"
            />
            <link
                rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css"
            />
            <link rel="stylesheet" href="snapshop.css" />
            <style
                dangerouslySetInnerHTML={{
                    __html:
                        "\n            nav .image {\n                width: 80px;\n                height: 80px;\n            }\n            .hello {\n                width: 100%;\n                height: 200px;\n                background-color: rgba(83, 113, 255, 0.12);\n            }\n\n            .h {\n                width: 100%;\n                \n            }\n \n            .terms {\n                margin-left: 10%;\n                margin-right: 10%;\n                margin-top: -100px;\n                background-color: #fff;\n            }\n\n            .accountButton {\n                width: 100px;\n                height: 50px;\n            }\n\n            .accountButton .myicon {\n                width: 40px !important;\n                height: 40px !important;\n            }\n\n            .dropdown:hover>.dropdown-menu {\n                display: block;\n            }\n\n            \n        "
                }}
            />
            <main>
                <div className="h d-flex justify-content-center align-items-center">
                    <div className="head1">
                        <div className="hello">
                            <br />
                            <h1 className="text-center fw-bold">Terms and Conditions</h1>
                        </div>
                        <div className="terms">
                            <h3 className="pt-5 mb-5 text-center fw-bold">General Terms </h3>
                            <hr />
                            <p className="lead mt-4">
                                This User Agreement is a contract between the user and the service provider of SnapShop.org.
                                You must read, agree to, and accept all of the terms and conditions contained in this Agreement
                                in order to use our website located at www.snapshop.com, all affiliated websites, including mobile
                                websites and applications, owned and operated by us.
                            </p>
                            <p className="lead mt-4">
                                This agreement includes and incorporates by reference the following: Privacy Policy: Guidlines:
                                and Project Contracts, as such agreements may be in effect and modified by SnapShop member fromt
                                time to time. This Agreement and the Terms of Service govern the use of the Servic es offered by
                                SnapShop on out Site.
                            </p>

                            <p className="lead mt-4">
                                Subject to teh conditions set forth herein, SnapShop may, in its sole discretion, change, modify,
                                add to supplement, delete or amend this Agreement and the other Terms of Service at any time by
                                posting a revised version on the site effective with or without prior notice.
                            </p>

                            <p className="lead mt-4">
                                The Terms and Conditions follows:

                                <ul className="lead mb-5">
                                    <li>
                                        The user must be aware of simple listing template to ease your workload but you must follow the description given while adding the item.
                                    </li>
                                    <li>
                                        Your product must be uploaded with a valid image, otherwise the member will have to suspend your items from the website.
                                    </li>
                                    <li>
                                        The product’s location must be the location of Bhutan only.

                                    </li>
                                    <li>
                                        You must know that we have no currency converter rather you can use the Bhutan currency Ngultrum.

                                    </li>
                                    <li>
                                        The shipping charge will be fixed for all product. The details of shipping will be given in the home page footer. Please visit there kindly.

                                    </li>
                                    <li>
                                        No scam. If found we will disable those source as well as the member who spreads those scam.

                                    </li>
                                    <li>
                                        You must build a sense of working together. Be cooperative.

                                    </li>
                                    <li>
                                        Love your customer. And love your seller.
                                    </li>
                                </ul>
                            </p>
                            <p className="lead mt-4"> 
                                By accessing and placing an order with snapshop, you confirm
                                that you are in agreement with and bound by the terms of service
                                contained in the Terms &amp; Conditions outlined below. These
                                terms apply to the entire website and any email or other type of
                                communication between you and snapshop.com{" "}
                            </p>
                            <p />
                        </div>
                    </div>
                </div>
            </main>
        </>
    );
}

export default Terms;