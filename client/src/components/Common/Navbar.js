import React, { useState } from 'react';
import { useEffect } from 'react';
import { Link, Outlet, useLocation, useNavigate } from "react-router-dom";

const Navbar = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);

    useEffect(() => {
        if (location.pathname === "/") {
            navigate("/Home")
        }
    }, [])

    const handleDropdownToggle = () => {
        setIsDropdownOpen(!isDropdownOpen);
    }

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light nav-shadow" style={{ backgroundColor: '#fd8231' }}>
                <div className="container">
                    <Link className="navbar-brand" to="/">
                        <img className="logo-img h-50 w-50" src="../images/SnapShop.png" alt="logo" />
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li className="nav-item text-white">
                                <Link className="nav-link items text-white " aria-current="page" to="Home">Home</Link>
                            </li>
                            <li className={`nav-item text-white dropdown ${isDropdownOpen ? 'show' : ''}`} onMouseEnter={handleDropdownToggle} onMouseLeave={handleDropdownToggle}>
                                <Link className="nav-link items text-white dropdown-toggle" to="#" id="clothesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded={isDropdownOpen}>
                                    Category
                                </Link>
                                <ul className={`dropdown-menu ${isDropdownOpen ? 'show' : ''}`} aria-labelledby="CategoryDropdown">
                                    <li><Link className="dropdown-item" to="Clothes">Clothes</Link></li>
                                    <li><Link className="dropdown-item" to="Cosmetics">Cosmetics</Link></li>
                                    <li><Link className="dropdown-item" to="Textiles">Textiles</Link></li>
                                </ul>
                            </li>
                            <li className="nav-item text-white">
                                <Link className="nav-link items text-white" to="Profile"><i className="fas fa-user fontawesome-style"></i></Link>
                            </li>
                            <li className="nav-item text-white">
                                <Link className="nav-link items text-white" to="#">
                                    <i className="far fa-heart" style={{ color: "#ffffff" }}></i>
                                </Link>
                            </li>

                            <li className="nav-item text-white">
                                <Link className="nav-link items" to="#">
                                    <i class="fas fa-sign-out-alt" aria-hidden="true" style={{ color: "#ffffff" }}></i>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <Outlet />
        </>
    )
}

export default Navbar;
