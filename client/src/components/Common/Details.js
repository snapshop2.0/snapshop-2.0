import React, { useEffect, useState } from 'react';
import { Link, useParams, useLocation } from 'react-router-dom';
import Heart from 'react-animated-heart';
import axios from 'axios';

const API_BASE_URL = 'http://localhost:5000';
const Details = () => {
  const { id } = useParams();
  const location = useLocation();

  const [details, setDetails] = useState({});
  const [isClick, setClick] = useState(false);

  const getDetails = async () => {
    try {
      const response = await axios.get(`${API_BASE_URL}/getProducts/clothes/${id}`);
      setDetails(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getDetails();
  }, []);

  return (
    <>
      <div className="container detail-page">
        <div className="detail-box">
          <div className="row">
            <div className="col-12 col-lg-4">
              <img src={`${API_BASE_URL}/uploads/${details.image}`} alt="clothe" />
            </div>
            <div className="col-12 col-lg-8 detail-info">
              <h1>{details.name}</h1>
              <h3>{details.description}</h3>
              <h2>{details.price}</h2>
              <h4>Quantity: {details.quantity}</h4>
              <Heart isClick={isClick} onClick={() => setClick(!isClick)} />
              <Link to="/ShopContext" className="btn-sub">
                Contact Us
              </Link>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Details;
