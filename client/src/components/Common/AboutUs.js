import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';

const AboutUs = () => {
  return (
    <Container>
      <Row>
        <Col>
          <h1>About Us</h1>
        </Col>
      </Row>
      <Row>
        <Col md={6}>
          <Image src="https://via.placeholder.com/300" fluid />
        </Col>
        <Col md={6}>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, elit vel lacinia bibendum, nunc urna bibendum massa, vel malesuada ex velit vel dolor. Sed euismod, elit vel lacinia bibendum, nunc urna bibendum massa, vel malesuada ex velit vel dolor.</p>
          <p>Nulla facilisi. Sed euismod, elit vel lacinia bibendum, nunc urna bibendum massa, vel malesuada ex velit vel dolor. Sed euismod, elit vel lacinia bibendum, nunc urna bibendum massa, vel malesuada ex velit vel dolor.</p>
        </Col>
      </Row>
    </Container>
  );
};

export default AboutUs;