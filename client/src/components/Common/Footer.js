import React from 'react';
import { Link } from "react-router-dom";
import "./Footer.css";

const Footer = () => {
    return (
        <>
            <footer className="footer ">
                <div className="container ">
                    <div className="row ">
                        <div className="col-6 col-md-3 ">
                            <img className="logo-img h-100 w-100" src="../images/SnapShop.png" alt="logo" />
                   
                        </div>
                        <div className="col-6 col-md-3 ">
                            
                            <h2 className="brand f-brand"></h2>
                        </div>
                        <div className="col-6 col-md-6 ">
                            <ul className="contactus">
                            <li className="contactus text-white">
                                <Link className="conatctus items text-white "  to="AboutUs">Our Story</Link>
                            </li>
                            <li className="contactus text-white">
                                <Link className="conatctus items text-white "  to="Contact">Contact Us</Link>
                            </li>
                            <li className="contactus text-white">
                                <Link className="conatctus items text-white "  to="Terms">Terms & Conditions</Link>
                            </li>
                        
                            </ul>
                        </div>
                    </div>
                    <div className="row copyright">
                        <div className="col-12 col-lg-6">
                            <p className="copy">SnapShop &copy; 2023, All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}

export default Footer;