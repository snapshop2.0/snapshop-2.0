import React from 'react';
import "./Footer.css";
import { Link } from 'react-router-dom';
import cosmetics from '../../assests/cosemetic.jpg';
import Cloth from '../../assests/Clothe.jpg';
import Textile from '../../assests/Textile.jpg'
import clotheicon from '../../assests/clothes11.png';
import cosmeticicon from '../../assests/cosmetics11.png';
import textileicon from '../../assests/textiles11.png';
import Footer from './Footer';

const Home = () => {
  return (
    <>
      <section className="container" style={{ marginTop: 100 }}>
        <div className="row d-flex align-items-center">
          <div className="hero-text col-12 col-lg-6" >
            <h1 className="heading">
              <span className="heading-design">WELCOME</span> BACK
            </h1>
            <p className="main-hero-para">
              Your Online Buddy,
              <br />
              Just A Click Of A Button Or A Swipe Of Your Finger
            </p>
          </div>
        </div>
      </section>

      <section id="products" className="partners-products container">
        <h1 className="title">Categories</h1>
        <div className="partners-logo-products products row">
          <div className="category col-12 col-md-6" style={{ backgroundColor: '#fd8231' }}>
          <img
              src={clotheicon}
              alt="products"
              className="partner-product"
            />
            <h2 className="category-tag">Clothes</h2>
            <Link to="Clothes" className="btn-sub">
              View
            </Link>
          </div>
          <div className="category col-12 col-md-6" style={{ backgroundColor: '#fd8231' }}>
            <img
              src={cosmeticicon}
              alt="products"
              className="partner-product"
            />
            <h2 className="category-tag">Cosmetics</h2>
            <Link to="Cosmetics" className="btn-sub">
              View
            </Link>
          </div>
          <div className="category col-12 col-md-6" style={{ backgroundColor: '#fd8231' }}>
            <img
              src={textileicon}
              alt="products"
              className="partner-product"
            />
            <h2 className="category-tag">Textile</h2>
            <Link to="Textiles" className="btn-sub">
              View
            </Link>
          </div>
        </div>
      </section>

      <div
        id="carouselExampleCaptions"
        className="carousel slide container rounded mb-5 pb-5"
        style={{ minHeight: 200 }}
      >
        <div className="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide-to="0"
            className="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleCaptions"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          ></button>
        </div>
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img
              src={cosmetics}
              className="d-block w-100 rounded"
              alt="..."
            />

            <div className="carousel-caption d-none d-md-block">
              <h5>First slide label</h5>
              <p>Some representative placeholder content for the first slide.</p>
            </div>
          </div>

          <div className="carousel-item">
            <img
              src={Cloth}
              className="d-block w-100 rounded"
              alt="..."
            />
            <div className="carousel-caption d-none d-md-block">
              <h5>Second slide label</h5>
              <p>Some representative placeholder content for the second slide.</p>
            </div>
          </div>
          <div className="carousel-item">
            <img
              src={Textile}
              className="d-block w-100 rounded"
              alt="..."
            />
            <div className="carousel-caption d-none d-md-block">
              <h5>Third slide label</h5>
              <p>Some representative placeholder content for the third slide.</p>
            </div>
          </div>
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleCaptions"
          data-bs-slide="prev"
        >
          <span className="carousel-control-prev-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleCaptions"
          data-bs-slide="next"
        >
          <span className="carousel-control-next-icon" aria-hidden="true"></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>
      <Footer />
    </>
  );
};

export default Home;
