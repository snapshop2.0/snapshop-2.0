const express = require("express");
const app = express();
const cors = require("cors");
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');
const pool = require('./db');
const ValidInfo = require('./middleware/ValidInfo');

app.use(cors());
app.use(express.json());

app.get("/", (req,res) => {
    res.json("hello")
})

app.use('/auth',require('./routes/jwtAuth'))

function generatePassword(length) {
    const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#$%*&!';
    let password = '';
    for (let i = 0; i < length; i++) {
      password += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return password;
}


app.post("/sendPassword", async (req,res) => {
    const { email } = req.body;
    const password = generatePassword(10);
    console.log(password)

    const saltRound = 10
    const salt = await bcrypt.genSalt(saltRound);
    const bcryptPassword = await bcrypt.hash(password,salt);
    console.log(bcryptPassword)

    try {

        const user = await pool.query("SELECT * FROM users WHERE email = $1",[email]);

        if (user.rows.length === 0) {
            console.log("user not found");
            res.status(500).json("User not found")
        }

        const transporter = nodemailer.createTransport({
            host: 'smtp.zoho.com',
            port: 465,
            secure: true,
            auth: {
              user: 'snapshopprojectcs@zohomail.com',
              pass: 'Jigme12@3'
            }
        });

        const mailOptions = {
            from: 'snapshopprojectcs@zohomail.com',
            to: email,
            subject: "Snapshop",
            text: `Your password is ${password}`
        };
          
        transporter.sendMail(mailOptions, async (error, info) => {
            if (error) {
              console.log(error);
            } else {
              console.log('Email sent: ' + info.response);
              const updatingPassword = await pool.query("UPDATE users SET password = $1 WHERE email = $2 RETURNING *",[bcryptPassword,email])

              if (updatingPassword.rows.length !== 0) {
                res.status(200).json({success:true,password:password})
              } else {
                res.status(401).json("false")
              }
            }
        });

    } catch (error) {
        console.log(error.message);
        res.status(500).json({error:error.message});
    }
})

const generateOTP = () => {
    const digits = "0123456789";
    let otp = '';

    for (let i = 0; i < 4; i++) {
        otp += digits[Math.floor(Math.random() * 10)];
    }

    return otp;
}


app.post("/sendOTP",ValidInfo, async (req,res) => {
    try {
        const {email} = req.body;
        const otp = generateOTP();
        console.log(otp);

        const transporter = nodemailer.createTransport({
            host: 'smtp.zoho.com',
            port: 465,
            secure: true,
            auth: {
                user: 'snapshopprojectcs@zohomail.com',
                pass: 'Jigme12@3'
            }
        });

        const mailOptions = {
            from: 'snapshopprojectcs@zohomail.com',
            to: email,
            subject: "Snapshop",
            text: `Your OTP is ${otp}`
        };

        transporter.sendMail(mailOptions, async (error, info) => {
            if (error) {
                console.log(error.message);
              return res.json(error.message);
            } else {
             return res.json({otp:otp, success:true})
            }
        });

    } catch (error) {
        console.log(error.message);
        return res.json({error:false});
    }
})



app.listen(5000, () => {
    console.log(`Server has started on port 5000`);
})



const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/uploads/');
    },
    filename: (req, file, cb) => {
      cb(null, Date.now() + path.extname(file.originalname));
    },
  });
  
  // Init upload middleware
  const upload = multer({
    storage: storage,
    fileFilter: function (req, file, cb) {
      checkFileType(file, cb);
    }
  }).single('image');
  
  // Check file type function
  function checkFileType(file, cb) {
    const filetypes = /jpeg|jpg|png|gif/;
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    const mimetype = filetypes.test(file.mimetype);
    if (mimetype && extname) {
      return cb(null, true);
    } else {
      cb('Error: Images only!');
    }
  }
  
  //Upload Products
  app.post('/uploadProducts', upload, async (req, res) => {
    const { title, category, quantity, location, description, price, owner, number } = req.body;
    const { filename } = req.file;
  
  
    try {
      const result = await pool.query('INSERT INTO products(name,description,price,owner,image,category,quantity,location,number) values ($1,$2,$3,$4,$5,$6,$7,$8,$9)'
        , [title, description, price, owner, filename, category, quantity, location, number])
      res.status(201).json({ message: 'Successfully Added' })
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Internal server error' });
    }
  })
  
  app.get('/getProducts/clothes', async (req, res) => {
    try {
      const result = await pool.query('SELECT * FROM products WHERE category=$1', ['Clothes']);
      res.status(201).json(result.rows)
      console.log(result.rows);
    } catch (error) {
      console.log(error);
    }
  })
  app.get('/getProducts/clothes/:id', async (req, res) => {
    try {
      const result = await pool.query('SELECT * FROM products WHERE pid=$1', [req.params.id]);
      res.status(201).json(result.rows[0]);
    } catch (error) {
      console.log(error)
    }
  })
  
  app.get('/getProducts/cosmetics', async (req, res) => {
    try {
      const result = await pool.query('SELECT * FROM products WHERE category=$1', ['Cosmetics']);
      res.status(201).json(result.rows)
      console.log(result.rows);
    } catch (error) {
      console.log(error);
    }
  })
  app.get('/getProducts/cosemetics/:id', async (req, res) => {
    try {
      const result = await pool.query('SELECT * FROM products WHERE pid=$1', [req.params.id]);
      res.status(201).json(result.rows[0]);
    } catch (error) {
      console.log(error)
    }
  })
  
  app.get('/getProducts/textile', async (req, res) => {
    try {
      const result = await pool.query('SELECT * FROM products WHERE category=$1', ['Textile']);
      res.status(201).json(result.rows)
      console.log(result.rows);
    } catch (error) {
      console.log(error);
    }
  })
  app.get('/getProducts/textile/:uid', async (req, res) => {
    try {
      const result = await pool.query('SELECT * FROM products WHERE pid=$1', [req.params.uid]);
      res.status(201).json(result.rows[0]);
    } catch (error) {
      console.log(error)
    }
  })
  
  app.get('/getYourProduct/:id', async (req, res) => {
    try {
      const result = await pool.query('SELECT * FROM products WHERE owner=$1', [req.params.id]);
      res.status(201).json(result.rows);
    } catch (error) {
      console.log(error)
    }
  })
  
  app.post('/deleteProduct/:pid', async (req, res) => {
    const { uid } = req.body;
    try {
      await pool.query('DELETE FROM products WHERE pid=$1 and owner=$2', [req.params.pid, uid])
  
    } catch (error) {
      console.log(error);
    }
  })
  
  
  
  app.listen(5000, () => {
    console.log(`Server has started on port 5000`);
  })
  