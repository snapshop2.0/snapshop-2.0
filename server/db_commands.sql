CREATE TABLE users(
  uid SERIAL PRIMARY KEY NOT NULL,
  email VARCHAR(255) NOT NULL UNIQUE,
  name VARCHAR(255) NOT NULL,
  password CHAR(60) NOT NULL,
  gender VARCHAR(255) NOT NULL,
  phone_number integer NOT NULL,
  products integer[] NOT NULL,
  doc TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE products(
  pid SERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255) NOT NULL,
  price integer NOT NULL,
  owner integer NOT NULL,
  doc TIMESTAMP NOT NULL DEFAULT NOW(),
  CONSTRAINT fk_owner
    FOREIGN KEY(owner)
        REFERENCES users(uid)
);

