module.exports = function (req,res,next) {
    const {email,name,password,confirm_password,gender,phone} = req.body;
    const email_pattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    if (req.path === "/register") {

        if (email === "") {
            console.log("running")
            return res.status(401).json("Email should not be empty")
        } else if (!email_pattern.test(email)) {
            return res.status(401).json("Invalid email format")
        } else if (password === "") {
            return res.status(401).json("Password should not be empty")
        } 
         else if (confirm_password === "" || confirm_password !== password) {
            return res.status(401).json("Passwords do not match")
        } else if (!gender) {
            return res.status(401).json("No gender");
        } else if (!phone) {
            return res.status(401).json("No phone number");
        }

    }  else if (req.path === "/login") {
        if (![email,password].every(Boolean)) {
            return res.status(401).json("Missing credentials");
        } else if (!email_pattern.test(email)) {
            return res.status(401).json("Invalid Email");
        }
    }
    next();
}

