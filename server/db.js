const Pool = require("pg").Pool;
require("dotenv").config();

const pool = new Pool({
    connectionString: process.env.DATABASE_URL
})

pool.connect((err, client, release) => {
    if (err) {
        console.log(err.message);
        return;
    }

    console.log("Connected to psql");

    release();
})

module.exports = pool;